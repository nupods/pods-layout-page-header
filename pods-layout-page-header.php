<?php
/**
 * Plugin Name: PODS Page Header
 * Description: Add a filter to the_content to create a grid using ACF
 */

namespace PODS\LayoutPageHeader;

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


if (!class_exists('LayoutPageHeader')) {
    class LayoutPageHeader
    {

        // vars
        var $settings;


        /*
        *  __construct
        *
        *  A dummy constructor to ensure initialized once
        */

        function __construct() {

            /* Do nothing here */

        }


        /*
        *  initialize
        *
        *  The real constructor to initialize ACF
        */

        public function initialize()
        {

            // vars
            $this->settings = array(
                // basic
                'name'              => __('PODS LayoutPageHeader'),
                'version'           => '0.1',

                // urls
                'basename'          => plugin_basename( __FILE__ ),
                'path'              => plugin_dir_path( __FILE__ ),
                'dir'               => plugin_dir_url( __FILE__ ),
            );

            // actions
            add_action('init',  array($this, 'defineFields'), 20);
        }


        /*
        *  defineFields
        *
        *  Defines ACF's
        *
        *  @param   N/A
        *  @return  N/A
        */

        public function defineFields()
        {
            // Loop through potential locations
            $locations = [];
            foreach (get_post_types(['public' => true]) as $post_type) {
                $locations[] = array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => $post_type
                    )
                );
            }

            if (function_exists('acf_add_local_field_group')) {
                acf_add_local_field_group(array (
                    'key' => 'group_55afc0a91b9a4',
                    'title' => '[layout] Page Header',
                    'fields' => array (
                        array (
                            'key' => 'field_55afc117e4095',
                            'label' => 'Background Image',
                            'name' => 'image_background',
                            'type' => 'image',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => 'field_type-image field_key-field_55afc117e4095',
                                'id' => '',
                            ),
                            'return_format' => 'array',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                        ),
                        array (
                            'key' => 'field_55afc153e4096',
                            'label' => 'Background Color',
                            'name' => 'color_background',
                            'type' => 'color_picker',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => 'field_type-color_picker field_key-field_55afc153e4096',
                                'id' => '',
                            ),
                            'default_value' => '',
                        ),
                        array (
                            'key' => 'field_55afc163e4706',
                            'label' => 'Page Pre Title',
                            'name' => 'text_page_pretitle',
                            'type' => 'textarea',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => '',
                            'new_lines' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                        array (
                            'key' => 'field_55afc163e4097',
                            'label' => 'Page Title',
                            'name' => 'text_page_title',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => 'field_type-text field_key-field_55afc163e4097',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                        array (
                            'key' => 'field_55afc17de4098',
                            'label' => 'Page Subtitle',
                            'name' => 'text_page_subtitle',
                            'type' => 'textarea',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => 'field_type-text field_key-field_55afc17de4098',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                    ),
                    'location' => $locations,
                    'menu_order' => 2,
                    'position' => 'side',
                    'style' => 'default',
                    'label_placement' => 'top',
                    'instruction_placement' => 'label',
                    'hide_on_screen' => '',
                    'active' => 1,
                    'description' => '',
                ));
            }
        }
    }
}


/**
 * Instantiates the LayoutPageHeader
 */
function runLayoutPageHeader()
{
    $run = new LayoutPageHeader();
    $run->initialize();
}

runLayoutPageHeader();


/**
 * Page header sytles
 */
function pageHeaderStyles($field = "get_field") {
    if ( !is_archive() ) {
        $style = 'style="';

        if ( isset($field('image_background')['url']) ) {
            $style .= 'background-image:url(\''. $field('image_background')['url'] .'\');';
        }

        if ( $field('color_background') ) {
            $style .= 'background-color:'. $field('color_background') .";";
        }

        if ( $field('color_font') ) {
            $style .= 'color:'. $field('color_font') .";";
        }

        $style .='"';
        return $style;
    }
}

/**
 * Page header title
 */
function pageHeaderTitles($field = "get_field") {
    if ($field('text_page_title')) {
        $page_title = '';

        if ($field('text_page_pretitle')) {
            $page_title .= '<h6 class="page-pretitle">'. $field('text_page_pretitle') .'</h6>';
        }

        $page_title .= '<h1 class="page-title">'. $field('text_page_title') .'</h1>';

        if ($field('text_page_subtitle')) {
            $page_title .= '<h2 class="page-subtitle">'. $field('text_page_subtitle') .'</h2>';
        }

        return $page_title;
    }
}
